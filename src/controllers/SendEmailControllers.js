const nodemailer = require("nodemailer");
require("dotenv").config();

class SendEmailControllers {
  init = async (request, response) => {
    response.send("Esta on");
  };
  send = async (request, response) => {
    const {nome,telefone,email} = request.body;
    //
    console.log(process.env.LG_USER);
    // return response.json(body);

    var transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASS,
      },
    });

    var mailOptions = {
      from: process.env.MAIL_FROM,
      to: process.env.MAIL_TO,
      subject: "Site Lançamento",
      text: `
      nome: ${nome}
      telefone: ${telefone}
      email: ${email}
      `,
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        return response.send(error);
      } else {
        return response.send("Email sent: " + info.response);
      }
    });
  };
}
module.exports = { SendEmailControllers };
