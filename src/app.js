const express = require("express");
var cors = require('cors')
const PORT = process.env.PORT || 5000;
const router = require("./routes");

const app = express();
app.use(cors())

// app.use((req, res, next) => {
//   res.header("Access-Control-Allow-Origin", "*")
//   app.use(cors);
//   next();
// });
app.use(express.json());

app.use(router);

app.listen(PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`);
});
