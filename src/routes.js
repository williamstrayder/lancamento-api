const { Router } = require("express");
const { SendEmailControllers } = require("./controllers/SendEmailControllers");
const router = Router();
const sendemailcontrollers = new SendEmailControllers();

router.post("/", sendemailcontrollers.send);
router.get("/", sendemailcontrollers.init);
// router.post("/", (req,res)=>{
//      res.send("funfo")
// });

module.exports = router